import sys
import os
from setuptools import setup, find_packages

requirements = ["brewtils"]

setup_args = {
    "name": "MC-MNTOS Calculator",
    "description": "MC-MNTOS Calculator",
    "version": "1.0.0",
    "pacakges": find_packages(exclude=["test", "test.*"]),
    "install_requires": requirements,
}

setup(**setup_args)
