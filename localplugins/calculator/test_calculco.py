import pytest
from main import Calculator

c = Calculator()


def test_add():
    ret = c.add(1, 2)
    assert ret == 3


def test_substract():
    ret = c.substract(1, 2)
    assert ret == -1


def test_multiply():
    ret = c.multiply(2, 2)
    assert ret == 4


def test_divide():
    ret = c.divide(2, 2)
    assert ret == 1


def test_divide_zero():
    with pytest.raises(ValueError):
        c.divide(2, 0)


def test_fact():
    ret = c.fact(0)
    assert ret == 1
