import sys
from brewtils.decorators import system, parameter, command
from brewtils.plugin import RemotePlugin
from bg_utils.local_plugin import LocalPlugin
from math import factorial


@system
class Calculator(object):
    """Megasoft Calculator MC-MNTOS

    1. Perform basic calculations (add, subtract, multiple, divide).
    2. Perform factorial calculation.

    3. In order to eliminate any potential bugs in the software,
    automated tests must be written and run to ensure the integrity of
    the application.

    4. The calculator will be triggered and interacted with on the CLI.
    5. The calculator will also be triggered via a web API.

    """

    @command(description="MC-MNTOS pretty Addition!")
    @parameter(key="x", type=float, description="X Parameter indeed")
    @parameter(key="y", type=float, description="Y Parameter indeed")
    def add(self, x, y):
        return x + y

    @command(description="MC-MNTOS pretty superfluous substraction!")
    @parameter(key="x", type=float, description="X Parameter indeed")
    @parameter(key="y", type=float, description="Y Parameter indeed")
    def substract(self, x, y):
        return x - y

    @command(description="MC-MNTOS pretty Multiplication!")
    @parameter(key="x", type=float, description="X Parameter indeed")
    @parameter(key="y", type=float, description="Y Parameter indeed")
    def multiply(self, x, y):
        return x * y

    @command(description="MC-MNTOS pretty Division!")
    @parameter(key="x", type=float, description="X Parameter indeed")
    @parameter(
        key="y", type=float, description="Y Parameter indeed")
    def divide(self, x, y):
        if y != 0:
            return x / y
        raise ValueError('zero division error!')

    @command(description="MC-MNTOS pretty Factorial!")
    @parameter(key="n", type=int, description="n Parameter indeed")
    def fact(self, n):
        return factorial(n)


def remote_main():
    """Running the plugin in remote mode"""
    c = Calculator()
    p = RemotePlugin(
        c,
        name="MC-MNTOS",
        version="1.0.0",
        bg_host="brew-view",
        bg_port=2337,
        ssl_enabled=False,
    )
    p.run()
    c.add(21, 21)


def main():
    """Running the plugin in local mode (default)"""
    client = Calculator()
    plugin = LocalPlugin(client)
    plugin.run()


if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == "rest":
        main()
    else:
        print("Interactive CLI mode Type Ctrl+C to exit")
        c = Calculator()
        while True:
            operator = input("Operator to use [+, *, /, !] : ")
            if operator not in ['+', '*', '/', '!']:
                print('Please type a valid operator [+, *, /, !]')
                continue
            if operator in ['+', '*', '/']:
                x = input('Please enter x: ')
                y = input('Please enter y: ')
                try:
                    x = float(x)
                except ValueError:
                    raise ValueError("Float please!")
                except Exception:
                    raise ValueError("Something went wrong!",
                                     sys.exc_info()[0])
                try:
                    y = float(y)
                except ValueError:
                    raise ValueError("Float please!")
                except Exception:
                    raise ValueError("Something went wrong!",
                                     sys.exc_info()[0])

                if operator == '+':
                    print("Result: {}".format(c.add(x, y)))
                elif operator == '*':
                    print("Result: {}".format(c.multiply(x, y)))
                elif operator == '/':
                    print("Result: {}".format(c.divide(x, y)))

                else:
                    print("How come ?")

            if operator in ['!']:
                n = input('Please enter n: ')
                try:
                    n = float(n)
                except ValueError:
                    raise ValueError("Ineteger please!")
                except Exception:
                    raise ValueError("Something went wrong!",
                                     sys.exc_info()[0])

                print("Result: {}".format(c.fact(n)))
