Calculator MC-MNTOS
===================

[![pipeline status](https://gitlab.com/wilane/mntos/badges/master/pipeline.svg)](https://gitlab.com/wilane/mntos/commits/master) [![coverage report](https://gitlab.com/wilane/mntos/badges/master/coverage.svg)](https://gitlab.com/wilane/mntos/commits/master)

The Calculator is built as a [Beer Garden Plugin](https://beer-garden.io/). The Calculator source code is mainly in [main.py](localplugins/calculator/main.py)

Quickstart
----------

Web Interface / Doc
*******************

- Install docker
- clone the repo (`git clone https://gitlab.com/wilane/mntos.git`)
- cd to directory and `docker-compose up -d`
- point your browser to http://localhost:2337 (give it few seconds if the plugins are not loaded)
- `docker-compose logs -f`

The Logs should read something like:

---

```
bartender_1   | 2019-05-20 10:13:10,778 - bartender.local_plugins.monitor - INFO - Local Plugin Monitor is started
bartender_1   | 2019-05-20 10:13:10,779 - bartender.monitor - INFO - Plugin Status Monitor is started
bartender_1   | 2019-05-20 10:13:10,780 - bartender.mongo_pruner - INFO - Mongo Pruner is started
bartender_1   | 2019-05-20 10:13:10,780 - bartender.app - INFO - Loading all local plugins...
bartender_1   | 2019-05-20 10:13:10,822 - bartender.local_plugins.registry - INFO - Registering Plugin Calc/MNTOS[default]-1.0.0 in the registry.
bartender_1   | 2019-05-20 10:13:10,822 - bartender.app - INFO - Starting all local plugins...
bartender_1   | 2019-05-20 10:13:10,822 - bartender.local_plugins.manager - INFO - Starting all plugins
bartender_1   | 2019-05-20 10:13:10,829 - bartender.local_plugins.manager - INFO - Starting plugin Calc/MNTOS[default]-1.0.0
bartender_1   | 2019-05-20 10:13:10,839 - Calc/MNTOS[default]-1.0.0 - INFO - Starting plugin Calc/MNTOS[default]-1.0.0 subprocess: ['/usr/local/bin/python', 'main.py', 'rest']
bartender_1   | 2019-05-20 10:13:10,842 - bartender.local_plugins.manager - INFO - Finished starting plugins.
bartender_1   | 2019-05-20 10:13:10,868 - bartender.app - INFO - Bartender started
bartender_1   | 2019-05-20 10:13:11,791 - bartender.thrift.handler - INFO - Initializing instance Calc/MNTOS[default]-1.0.0
bartender_1   | 2019-05-20 10:13:11,891 - brewtils.plugin - INFO - Plugin Calc/MNTOS[default]-1.0.0 has started
```
---

![Web](images/Web.gif "Web Interface/Doc")

CLI
***

- Install docker
- clone the repo (`git clone https://gitlab.com/wilane/mntos.git`)
- cd to directory and `docker-compose up -d`
- Run the interactive cal:

  ::

     $ `docker-compose exec bartender python /plugins/calculator/main.py`

![CLI](images/Interactive.gif)

Web API (curl, Insomnia)
************************


- Install docker
- clone the repo (`git clone https://gitlab.com/wilane/mntos.git`)
- cd to directory and `docker-compose up -d`

::

    $ curl -X POST -H "accept: application/json" -H "Content-Type: application/json" -d '{"system": "Calc/MNTOS", "system_version": "1.0.0", "command": "add", "instance_name": "default",  "comment": "","parameters": {"x": 123, "y": 142 }, "output_type": "STRING"}' "http://localhost:2337/api/v1/requests?blocking=true"
    
    $ curl -X POST -H "accept: application/json" -H "Content-Type: application/json" -d '{"system": "Calc/MNTOS", "system_version": "1.0.0", "command": "multiply", "instance_name": "default",  "comment": "","parameters": {"x": 123, "y": 142 }, "output_type": "STRING"}' "http://localhost:2337/api/v1/requests?blocking=true"

    $ curl -X POST -H "accept: application/json" -H "Content-Type: application/json" -d '{"system": "Calc/MNTOS", "system_version": "1.0.0", "command": "substract", "instance_name": "default",  "comment": "","parameters": {"x": 123, "y": 142 }, "output_type": "STRING"}' "http://localhost:2337/api/v1/requests?blocking=true"

    $ curl -X POST -H "accept: application/json" -H "Content-Type: application/json" -d '{"system": "Calc/MNTOS", "system_version": "1.0.0", "command": "divide", "instance_name": "default",  "comment": "","parameters": {"x": 123, "y": 142 }, "output_type": "STRING"}' "http://localhost:2337/api/v1/requests?blocking=true"

    $ curl -X POST -H "accept: application/json" -H "Content-Type: application/json" -d '{"system": "Calc/MNTOS", "system_version": "1.0.0", "command": "fact", "instance_name": "default",  "comment": "","parameters": {"n": 12}, "output_type": "STRING"}' "http://localhost:2337/api/v1/requests?blocking=true"

```javascript
{
  "created_at": 1558344430257,
  "has_parent": false,
  "parameters": {
    "n": 12
  },
  "system": "Calc/MNTOS",
  "instance_name": "default",
  "status": "SUCCESS",
  "output": "479001600",
  "updated_at": 1558344430315,
  "requester": "anonymous",
  "output_type": "STRING",
  "comment": "",
  "id": "5ce272ee896bb4a55af3ef7f",
  "parent": null,
  "command_type": "ACTION",
  "metadata": {},
  "system_version": "1.0.0",
  "error_class": null,
  "children": null,
  "command": "fact"
}
```

More info on [OpenAPI/Swagger documentation](http://localhost:2337/swagger/index.html?config=/config/swagger)

![Web API/curl](images/curl.gif)

![Web API/Insomnia](images/Insomnia.png)



Python
******

- Install docker
- clone the repo (`git clone https://gitlab.com/wilane/mntos.git`)
- cd to directory and `docker-compose up -d`
- pip install brewtils in your (virtual)env

::
   ```
   from brewtils.rest.system_client import SystemClient
   client = SystemClient("localhost", 2337, system_name="Calc/MNTOS")

   request = client.add(x=123, y=123)
   print(request.status)
   print(request.output)

   request = client.substract(x=123, y=123)
   print(request.status)
   print(request.output)
   ```

![Python](images/Python.gif)

Running tests with pytest w/cov
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

  $ `docker-compose exec bartender pytest --cov=plugins/calculator -rpP /plugins/calculator/test_calculco.py`


The test are run using gitlab CI/CD pipeline (badge added to this readme).